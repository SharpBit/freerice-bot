'''
MIT License

Copyright (c) 2018 SharpBit

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Note: Some of this code came from
https://github.com/sereneblue/freerice-bot
I have built upon it and made it for the vocab section.
'''


from bs4 import BeautifulSoup
import random
from time import sleep, strftime
import requests

session = requests.session()
# tells free rice what our "purpose" is
session.headers.update({
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
})


def login(username, password):
    '''Logs into freerice.com using requests.'''
    resp = session.get('http://freerice.com/user/login').text
    soup = BeautifulSoup(resp, 'lxml')
    resp = session.post('http://freerice.com/user/login', data={
        'name': username,  # inputs username
        'pass': password,  # inputs password (case sensitive)
        # gets the form build id and form id (hidden inputs) to log in
        'form_build_at': soup.find('input', {'name': 'form_build_id'})['value'],
        'form_id': soup.find('input', {'name': 'form_id'})['value'],
        'op': 'Log In'
    }).text
    # checks to see if the log in was successful
    if 'form action="/users/' in resp:
        return True
    else:
        return False


def do_vocab_questions():
    '''The actual good stuff. It does the problems for you.'''
    # CSS Selectors
    s_desc = '#filters-0 > div.synonym-description > strong'
    synonyms = '#filters-0 > div.relevancy-block > div > ul:nth-of-type(1) > li:nth-of-type({}) > a > span.text'

    game = session.post('http://freerice.com/game.php', data={
        'op': 'init',
        'token': 'undefined',
        'language': ''
    }).text
    while True:
        soup = BeautifulSoup(game, 'lxml')
        word = soup.find('b').text
        print(word)
        thesaurus = requests.get('http://www.thesaurus.com/browse/{}?s=t'.format(word)).text
        the_soup = BeautifulSoup(thesaurus, 'lxml')
        choice = the_soup.select(s_desc)[0].text
        print(choice)
        print([i.text for i in soup.find_all('a', {'class': 'answer-item'})])
        try:
            answer = [i['rel'] for i in soup.find_all('a', {'class': 'answer-item'}) if i.text == choice][0]
        except IndexError:
            # finds the first column on synonyms
            syns = []
            for i in range(1, 7):
                syns.append(the_soup.select(synonyms.format(i))[0].text)
            print(syns)
            for s in syns:
                try:
                    answer = [i['rel'] for i in soup.find_all('a', {'class': 'answer-item'}) if i.text == choice][0]
                except IndexError:
                    continue
        # test to see if the answer was found
        try:
            answer = answer
        except UnboundLocalError:
            # chooses random answer
            answer = random.choice([i['rel'] for i in soup.find_all('a', {'class': 'answer-item'})])
        game = session.post('http://freerice.com/game.php', data={
            'op': 'next',
            'token': soup.find('input', {'name': 'token'})['value'],
            'language': '',
            'answer': answer,
            'list': soup.find('input', {'name': 'list'})['value'],
            'nid': soup.find('input', {'name': 'nid'})['value'],
            'nb': 1
        }).text
        print('{}: {}- {}, +10 grains.'.format(strftime('%H:%M:%S', word, answer)))
        sleep(random.randrange(2, 6))


if __name__ == '__main__':
    print('FreeRice Bot by SharpBit')
    print('GitHub Link: https://github.com/SharpBit/freerice-bot')
    print('Note: This program does not save, use, or tamper with your account in any way.')
    print('I am not responsible for your account if it gets banned (if Free Rice even bans accounts) or you get in trouble.')
    print('Some of this came from https://github.com/sereneblue/freerice-bot')

    user = input('What is your username (NOT EMAIL)?\n> ')
    password = input('What is your password? (Passwords are case-sensitive)\n> ')

    if login(user, password):
        print('Log in was successful!')
        print('Doing vocabulary problems...')
        print('-------------------')
        do_vocab_questions()
    else:
        print('Log in failed. Make sure your username and password is correct.')
